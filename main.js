

var canvas;
var ctx;
var barOffsetX = 300;
var ballOffsetX = 390;
var ballOffsetY = 300;
var ballDirectionY = "bot";
var ballDirectionX = "none";
var direction;
var keys = new Array();
ballHitBox = [];
var blocks;

blocks = [];
//inizializzo i blocchi non rotti
for (var i = 0; i < 10; i++){

    blocks[i] = []; //inizializzo 2a dimensione

    for (var j = 0; j < 4; j++){

        blocks[i][j] = []; //inizializzo 3a dimensione
        blocks[i][j][2] = false;
    }
}

window.onload = function(){

    canvas = document.getElementById("breakBreakerCanvas");
    ctx = canvas.getContext("2d");
}

function undraw(){

    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

var blockMargin;
function drawBlocks(columns, rows){

    blockMargin = 60;

    for (var i = 0; i < columns; i++){

        for (var j = 0; j < rows; j++){
        
            if (!blocks[i][j][2]){
                ctx.fillStyle = "#1"+ j.toString() + j.toString() + j.toString() + j.toString() + j.toString();
                ctx.fillRect(i * canvas.width / columns, blockMargin + j * 30, canvas.width / 10, 30);
            }
            //metto le coordinate dei blocchuinell'array 3 dimension
            blocks[i][j][0] = (i * canvas.width / columns);
            blocks[i][j][1] = (j * 30);
        }
    }
}

function drawBall(){

    ctx.fillStyle = "#ffffff";
    ctx.beginPath();
    ctx.arc(ballOffsetX, ballOffsetY, 10, 0, 2 * Math.PI);
    ctx.closePath();
    ctx.fill();
}

function drawBar(){

    ctx.fillStyle = "#b5ff75";
    ctx.fillRect(barOffsetX, 500, 100, 10);
    ctx.stroke();
}

function drawScreen(){

    ctx.fillStyle = "#262626";
    ctx.fillRect(0,0,canvas.width,canvas.height);
}


//game over
function reset(){

    for (var i = 0; i < 10; i++){
    
        for (var j = 0; j < 4; j++){

            blocks[i][j][2] = false;
        }
    }

    barOffsetX = 300;
    ballOffsetX = 390;
    ballOffsetY = 300;
    ballDirectionX = "none";

}


//game tick 
window.setInterval(function(){

    //movimento palla
    if (ballDirectionY == "bot") ballOffsetY += 2;
    else ballOffsetY -= 2;
    if (ballDirectionX == "right1") ballOffsetX += 2;
    else if (ballDirectionX == "right2") ballOffsetX += 1;
    else if (ballDirectionX == "left1") ballOffsetX -= 2;
    else if (ballDirectionX == "left2") ballOffsetX -= 1;

    //se hitta il rettangolo
    if((ballOffsetY == canvas.height - 110) && (ballOffsetX > barOffsetX && ballOffsetX < barOffsetX + 100 || ballOffsetX + 10 > barOffsetX && ballOffsetX + 10  < barOffsetX + 100)){

        //il primo 25% della bar
        if (ballOffsetX < barOffsetX + 25) ballDirectionX = "left1";
        else if (ballOffsetX < barOffsetX + 50) ballDirectionX = "left2";
        else if (ballOffsetX < barOffsetX + 75) ballDirectionX = "right2";
        else if (ballOffsetX < barOffsetX + 100) ballDirectionX = "right1";

        ballDirectionY ="top";
    }

    // se hitta il bottom
    if (ballOffsetY + 10 > canvas.height) reset();

    // se hitta il top
    if (ballOffsetY - 10 < 0) ballDirectionY ="bot"
    
    // se hitta il lato destro
    if (ballOffsetX + 10 > canvas.width) ballDirectionX ="left2"

    // se hitta il lato sinistro
    if (ballOffsetX - 10 < 0) ballDirectionX ="right2";
    if (keys[39] && barOffsetX < canvas.width - 100) barOffsetX += 4;
    if (keys[37] && barOffsetX > 0) barOffsetX -= 4;
    

    for (var i = 0; i < 10; i++){

        for (var j = 0; j < 4; j++){

            //se hitto un blocco 
            if (ballOffsetX < blocks[i][j][0] + 90 && ballOffsetX > blocks[i][j][0]
                && ballOffsetY < blocks[i][j][1] + 90 && ballOffsetY > blocks[i][j][1]){

                    //cambio direzione (se il blocco non è già rotto)
                    if (!blocks[i][j][2]){

                        if (ballDirectionY == "top") ballDirectionY = "bot";
                        else ballDirectionY = "top";  
                    }

                    //non lo faccio più renderizzare
                    blocks[i][j][2] = true;
            }
        }
    }

    undraw(); //cancello tutto il canvas

    //ridisegno tutto
    drawScreen();
    drawBlocks(10,4);
    drawBar();
    drawBall();

}, 5);

//handlo la pressione di freccia destra e sinistra
window.onkeyup = function(e) { keys[e.keyCode] = false; }
window.onkeydown = function(e) { keys[e.keyCode] = true; }



